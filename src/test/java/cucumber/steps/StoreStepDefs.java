package cucumber.steps;

import cucumber.entities.Order;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

import java.util.Map;
import java.util.Random;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.junit.Assert.assertEquals;

public class StoreStepDefs {

    private static final String ORDER = "Order";
    private Response response;

    @DataTableType
    public Order createNewOrder(Map<String, String> entry) {
        return new Order(Math.abs(new Random().nextInt())
                , Integer.parseInt(entry.get("petId"))
                , Integer.parseInt(entry.get("quantity"))
                , entry.get("shipDate")
                , entry.get("status")
                , Boolean.parseBoolean(entry.get("complete")));
    }

    @Given("the following order for a pet")
    public void theFollowingOrderForAPet(Order order) {
        Serenity.setSessionVariable(ORDER).to(order);
    }

    @When("I successfully place the order")
    public void iSuccessFullyPlaceTheOrder() {
        placeOrderExpectingStatus(200);
    }

    private void placeOrderExpectingStatus(int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(ORDER))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/store/order")
                .when().post().prettyPeek();

        response.then().statusCode(status);
    }


    @Then("the order is found in the petstore")
    public void theOrderIsFoundInThePetstore() {
        Order order = (Order) Serenity.getCurrentSession().get(ORDER);

        findOrderExpectingStatus(order, 200);
        Order orderFromResponse = response.then().extract().body().as(Order.class);
        assertEquals(order, orderFromResponse);
    }

    private void findOrderExpectingStatus(Order order, int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/store/order/" + order.getId())
                .when().get().prettyPeek();
        response.then().statusCode(status);
    }

    @When("I unsuccessfully place the order")
    public void iUnsuccessfullyPlaceTheOrder() {
        placeOrderExpectingStatus(400);
    }

    @Then("the order is not found in the petstore")
    public void theOrderIsNotFoundInThePetstore() {
        Order order = (Order) Serenity.getCurrentSession().get(ORDER);

        findOrderExpectingStatus(order, 404);
    }

    @When("I successfully delete the order")
    public void iSuccessfullyDeleteTheOrder() {
        Order order = (Order) Serenity.getCurrentSession().get(ORDER);
        deleteOrderExpectingStatus(order.getId(), 200);
    }

    private void deleteOrderExpectingStatus(Integer id, int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/store/order/" + id)
                .when().delete().prettyPeek();
        response.then().statusCode(status);
    }

    @When("I unsuccessfully delete a non-existing order")
    public void iDeleteANonExistingOrder() {
        deleteOrderExpectingStatus(Math.abs(new Random().nextInt()), 404);
    }

    @When("I request the pet inventory")
    public void iRequestThePetInventory() {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/store/inventory/")
                .when().get().prettyPeek();
    }

    @Then("the pet inventory is successfully retrieved")
    public void thePetInventoryIsSuccessfullyRetrieved() {
        response.then().statusCode(200);
    }
}
