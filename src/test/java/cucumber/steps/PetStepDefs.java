package cucumber.steps;

import cucumber.entities.Category;
import cucumber.entities.Pet;
import cucumber.entities.Tag;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PetStepDefs {

    private static final String PET = "Pet";
    private static final String PETLIST = "PetList";
    private static final String UPDATED_PET = "UpdatedPet";
    private Response response;

    @DataTableType
    public Pet createNewPet(Map<String, String> entry) {
        return new Pet(Math.abs(new Random().nextInt()),
                new Category(Math.abs(new Random().nextInt())
                        , entry.get("categoryName"))
                , entry.get("petName")
                , Arrays.stream(entry.get("photoUrls").replaceAll("\\s+", "").split(",")).collect(Collectors.toList())
                , Arrays.stream(entry.get("tagName").replaceAll("\\s+", "").split(",")).map(tagName -> new Tag(Math.abs(new Random().nextInt()), tagName)).collect(Collectors.toList())
                , entry.get("status"));
    }

    @Given("I have the following pet")
    public void iHaveTheFollowingPet(Pet pet) {
        Serenity.setSessionVariable(PET).to(pet);
    }

    @Given("I have the following pets")
    public void iHaveTheFollowingPets(List<Pet> petList) {
        Serenity.setSessionVariable(PETLIST).to(petList);
    }

    @When("I successfully add the pet to the petstore")
    public void iSuccessfullyAddThePetToThePetstore() {
        addPetToThePetstoreExpectingStatus((Pet) Serenity.getCurrentSession().get(PET), 200);
    }

    @When("I successfully add the pets to the petstore")
    public void iSuccessfullyAddThePetsToThePetstore() {
        List<Pet> petList = (List<Pet>) Serenity.getCurrentSession().get(PETLIST);

        petList.forEach(pet -> addPetToThePetstoreExpectingStatus(pet, 200));
    }

    private void addPetToThePetstoreExpectingStatus(Pet pet, int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .body(pet)
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet")
                .when().post().prettyPeek();

        response.then().statusCode(status);
    }

    @When("I unsuccessfully add the pet to the petstore")
    public void iUnsuccessfullyAddThePetToThePetstore() {
        addPetToThePetstoreExpectingStatus((Pet) Serenity.getCurrentSession().get(PET), 405);
    }

    @Then("the pet is found in the petstore")
    public void thePetIsFoundInThePetstore() {
        Pet petToFind = (Pet) Serenity.getCurrentSession().get(PET);

        findPetInThePetStoreSuccessfully(petToFind);
    }

    private void findPetInThePetStoreSuccessfully(Pet petToFind) {
        findPetInThePetstoreExpectingStatus(petToFind, 200);

        Pet petFromResponse = response.then().extract().body().as(Pet.class);
        assertEquals(petToFind, petFromResponse);
    }

    private void findPetInThePetstoreExpectingStatus(Pet petToFind, int expectedStatusCode) {
        findPetInThePetstore(petToFind);

        response.then().statusCode(expectedStatusCode);
    }

    private void findPetInThePetstore(Pet petToFind) {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/" + petToFind.getId())
                .when().get();
    }

    @Then("the pet is not found in the petstore")
    public void thePetIsNotFoundInThePetstore() {
        Pet petToFind = (Pet) Serenity.getCurrentSession().get(PET);

        findPetInThePetstoreExpectingStatus(petToFind, 404);
    }

    @When("I upload a photo of my pet")
    public void iUploadAPhotoOfMyPet() {
        Pet petToFind = (Pet) Serenity.getCurrentSession().get(PET);

        response = given().accept("application/json")
                .contentType("multipart/form-data")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/" + petToFind.getId() + "/uploadImage")
                .formParam("additionalMetadata", "metaData")
                .multiPart(new File("src/test/resources/files/cat_photo.jpg"))
                .when().post().prettyPeek();

        response.then().statusCode(200);
    }

    @Then("the picture is uploaded successfully")
    public void thePictureIsUploadedSuccessfully() {
        response.then().assertThat()
                .statusCode(200)
                .body("type", equalTo("unknown"), "message", containsString("File uploaded"));
    }

    @When("I successfully update the existing pet to the following data")
    public void iSuccessfullyUpdateTheExistingPetToTheFollowingData(Pet updatedPet) {
        Pet firstPet = (Pet) Serenity.getCurrentSession().get(PET);
        updatedPet.setId(firstPet.getId());
        Serenity.setSessionVariable(UPDATED_PET).to(updatedPet);

        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(UPDATED_PET))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet")
                .when().put().prettyPeek();

        response.then().statusCode(200);
    }

    @Then("the updated pet is found in the petstore")
    public void theUpdatedPetIsFoundInThePetstore() {
        Pet petToFind = (Pet) Serenity.getCurrentSession().get(UPDATED_PET);

        findPetInThePetStoreSuccessfully(petToFind);
    }

    @Then("the updated pet is not found in the petstore")
    public void theUpdatedPetIsNotFoundInThePetstore() {
        Pet petToFind = (Pet) Serenity.getCurrentSession().get(UPDATED_PET);

        findPetInThePetstoreExpectingStatus(petToFind, 404);
    }

    @When("I unsuccessfully try to update a non-existing pet")
    public void iUnsuccessfullyTryToUpdateANonExistingPet(Pet updatedPet) {
        Serenity.setSessionVariable(UPDATED_PET).to(updatedPet);

        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(UPDATED_PET))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet")
                .when().put().prettyPeek();

        response.then().statusCode(404);
    }

    @When("I try to find all pets with status {}")
    public void iTryToFindAllPetsWithStatusAvailable(String statuses) {
        List<String> statusList = Arrays.asList(statuses.replaceAll("\\s+", "").split(","));
        RequestSpecification request = given().accept("application/json")
                .contentType("application/json")
                .queryParam("status", statusList)
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/findByStatus");
        response = request.when().get().prettyPeek();
    }

    @Then("my pet is returned")
    public void myPetIsReturned() {
        Pet myPet = (Pet) Serenity.getCurrentSession().get(PET);
        List<Object> returnedIds = response.then().statusCode(200).extract().jsonPath().get("id");
        assertTrue(returnedIds.contains(myPet.getId()));
    }

    @Then("my pets are returned")
    public void myPetsAreReturned() {
        List<Pet> petList = (List<Pet>) Serenity.getCurrentSession().get(PETLIST);
        List<Object> returnedIds = response.then().statusCode(200).extract().jsonPath().get("id");
        petList.stream().map(Pet::getId).forEach(id -> assertTrue(returnedIds.contains(id)));
    }

    @Then("response contains http status {int}")
    public void responseContainsHttpStatus(int status) {
        response.then().statusCode(status);
    }

    @When("I successfully update the existing pet name and status to the following data")
    public void iSuccessfullyUpdateTheExistingPetNameAndStatusToTheFollowingData(DataTable dataTable) {
        Map<String, String> data = dataTable.asMaps().get(0);
        String newName = data.get("petName");
        String newStatus = data.get("status");

        Pet firstPet = (Pet) Serenity.getCurrentSession().get(PET);
        firstPet.setName(newName);
        firstPet.setStatus(newStatus);
        Serenity.setSessionVariable(UPDATED_PET).to(firstPet);

        response = given().accept("application/json")
                .contentType("application/x-www-form-urlencoded")
                .formParam("name", newName)
                .formParam("status", newStatus)
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/" + firstPet.getId())
                .when().post().prettyPeek();

        response.then().statusCode(200);
    }

    @When("I successfully delete the pet from the petstore")
    public void iSuccessfullyDeleteThePetFromThePetstore() {
        Pet pet = (Pet) Serenity.getCurrentSession().get(PET);

        response = given().accept("application/json")
                .contentType("application/x-www-form-urlencoded")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/" + pet.getId())
                .when().delete().prettyPeek();

        response.then().statusCode(200);
    }


    @Then("I unsuccessfully delete a non-existing pet")
    public void iUnsuccessfullyDeleteANonExistingPet() {
        response = given().accept("application/json")
                .contentType("application/x-www-form-urlencoded")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/" + Math.abs(new Random().nextInt()))
                .when().delete().prettyPeek();

        response.then().statusCode(404);
    }

    @When("I try to find all pets with tags {}")
    public void iTryToFindAllPetsWithTagsTagTag(String tags) {
        List<String> tagList = Arrays.asList(tags.replaceAll("\\s+", "").split(","));
        RequestSpecification request = given().accept("application/json")
                .contentType("application/json")
                .queryParam("tags", tagList)
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/findByTags");
        response = request.when().get().prettyPeek();
    }
}
