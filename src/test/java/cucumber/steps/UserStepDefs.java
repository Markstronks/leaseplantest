package cucumber.steps;

import cucumber.entities.User;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;

import java.util.List;
import java.util.Map;
import java.util.Random;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

public class UserStepDefs {

    private static final String USER = "User";
    private static final String UPDATEDUSER = "UpdatedUser";
    private static final String USERLIST = "UserList";
    private Response response;

    @DataTableType
    public User createNewUser(Map<String, String> entry) {
        return new User(Math.abs(new Random().nextInt())
                , "username" + Math.abs(new Random().nextInt())
                , entry.get("firstName")
                , entry.get("lastName")
                , entry.get("email")
                , entry.get("password")
                , entry.get("phone")
                , Integer.parseInt(entry.get("userStatus")));
    }

    @Given("the following user")
    public void theFollowingUser(User user) {
        Serenity.setSessionVariable(USER).to(user);
    }

    @When("I add the single user")
    public void iAddTheSingleUser() {
        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(USER))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user")
                .when().post().prettyPeek();

        response.then().statusCode(200);
    }

    @Then("the user can be found")
    public void theUserCanBeFound() {
        User user = (User) Serenity.getCurrentSession().get(USER);

        userCanBeFound(user);
    }

    private void userCanBeFound(User user) {
        findUserExpectingStatus(user.getUsername(), 200);
        User userFromResponse = response.then().extract().body().as(User.class);
        assertEquals(user, userFromResponse);
    }

    private void findUserExpectingStatus(String username, int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/" + username)
                .when().get().prettyPeek();
        response.then().statusCode(status);
    }

    @Given("the following users")
    public void theFollowingUsers(List<User> userList) {
        Serenity.setSessionVariable(USERLIST).to(userList);
    }

    @When("I add users as an Array")
    public void iAddUsersAsAnArray() {
        List<User> userList = (List<User>) Serenity.getCurrentSession().get(USERLIST);

        response = given().accept("application/json")
                .contentType("application/json")
                .body(userList.toArray())
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/createWithArray")
                .when().post().prettyPeek();

        response.then().statusCode(200);
    }

    @Then("all users can be found")
    public void allUsersCanBeFound() {
        List<User> userList = (List<User>) Serenity.getCurrentSession().get(USERLIST);
        userList.forEach(this::userCanBeFound);
    }

    @When("I add users as a List")
    public void iAddUsersAsAList() {
        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(USERLIST))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/createWithList")
                .when().post().prettyPeek();

        response.then().statusCode(200);
    }

    @When("I update the user to the following values")
    public void iUpdateTheUserToTheFollowingValues(DataTable dataTable) {
        Map<String, String> data = dataTable.asMaps().get(0);

        User user = (User) Serenity.getCurrentSession().get(USER);
        user.setFirstName(data.get("firstName"));
        user.setLastName(data.get("lastName"));
        user.setEmail(data.get("email"));
        user.setPassword(data.get("password"));
        user.setPhone(data.get("phone"));
        user.setUserStatus(Integer.parseInt(data.get("userStatus")));


        Serenity.setSessionVariable(UPDATEDUSER).to(user);

        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(UPDATEDUSER))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/" + user.getUsername())
                .when().put().prettyPeek();

        response.then().statusCode(200);
    }

    @Then("the updated user can be found")
    public void theUpdatedUserCanBeFound() {
        User user = (User) Serenity.getCurrentSession().get(UPDATEDUSER);

        userCanBeFound(user);
    }

    @When("I successfully delete the user")
    public void iSuccessfullyDeleteTheUser() {
        User user = (User) Serenity.getCurrentSession().get(USER);

        deleteUserExpectingStatus(user.getUsername(), 200);
    }

    private void deleteUserExpectingStatus(String username, int status) {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/" + username)
                .when().delete().prettyPeek();

        response.then().statusCode(status);
    }

    @Then("the user cannot be found")
    public void theUserCannotBeFound() {
        User user = (User) Serenity.getCurrentSession().get(USER);
        findUserExpectingStatus(user.getUsername(), 404);
    }

    @Then("I unsuccessfully delete a non-existing user")
    public void iUnsuccessfullyDeleteANonExistingUser() {
        deleteUserExpectingStatus("DezeUserBestaatNiet", 404);
    }

    @When("the user logs in to the system")
    public void theUserLogsInToTheSystem() {
        User user = (User) Serenity.getCurrentSession().get(USER);

        login(user.getUsername(), user.getPassword());
    }

    @Then("a user session is returned")
    public void aUserSessionIsReturned() {
        response.then().statusCode(200).body("type", equalTo("unknown"), "message", containsString("logged in user session:"));
    }

    @When("the user logs in to the system with the wrong password")
    public void theUserLogsInToTheSystemWithTheWrongPassword() {
        User user = (User) Serenity.getCurrentSession().get(USER);

        login(user.getUsername(), "wrongPassword");
    }

    private void login(String username, String password) {
        response = given().accept("application/json")
                .contentType("application/json")
                .param("username", username)
                .param("password", password)
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/login")
                .when().get().prettyPeek();
    }

    @Then("no user session is returned")
    public void noUserSessionIsReturned() {
        response.then().statusCode(400);
    }

    @When("the user logs out")
    public void theUserLogsOut() {
        response = given().accept("application/json")
                .contentType("application/json")
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/logout")
                .when().get().prettyPeek();
    }

    @Then("the session has ended")
    public void theSessionHasEnded() {
        response.then().statusCode(200).body("type", equalTo("unknown"), "message", equalTo("ok"));
    }

    @When("I unsuccessfully try to update a non-existing user")
    public void iUnsuccessfullyTryToUpdateANonExistingUser(User user) {
        Serenity.setSessionVariable(UPDATEDUSER).to(user);

        response = given().accept("application/json")
                .contentType("application/json")
                .body(Serenity.getCurrentSession().get(UPDATEDUSER))
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/user/" + user.getUsername())
                .when().put().prettyPeek();

        response.then().statusCode(404);
    }

    @Then("the updated user cannot be found")
    public void theUpdatedUserCannotBeFound() {
        User user = (User) Serenity.getCurrentSession().get(UPDATEDUSER);
        findUserExpectingStatus(user.getUsername(), 404);
    }
}
