Feature: Pet Api

  Scenario: Add a new pet to the store
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    When I successfully add the pet to the petstore
    Then the pet is found in the petstore

    #Bug found: status does not comply with enum but still returns status 200
  Scenario: Adding a new pet to the store with an invalid status should throw an error
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status  |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | invalid |
    When I unsuccessfully add the pet to the petstore
    Then the pet is not found in the petstore

    #Bug found: name is mandatory but when it is not supplied the api still returns status 200
  Scenario: Adding a new pet to the store without a name should throw an error
    Given I have the following pet
      | categoryName | photoUrls                                                  | tagName | status  |
      | Dog          | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | invalid |
    When I unsuccessfully add the pet to the petstore
    Then the pet is not found in the petstore

    #Question: Shouldn't the image be stored as a photoUrl after the upload?
    #Question 2: Shouldn't an error be specified when uploading a photo for an Id that doesn't exist?
  Scenario: Upload an image of my pet
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I upload a photo of my pet
    Then the picture is uploaded successfully

  Scenario: Update an existing pet
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I successfully update the existing pet to the following data
      | categoryName | petName | photoUrls                                                        | tagName | status  |
      | Cat          | Eek     | https://static.tvtropes.org/pmwiki/pub/images/eekthecat_1947.jpg | tag2    | pending |
    Then the updated pet is found in the petstore

   #Bug found: Non-existing pet used in an update should return a 404.
  Scenario: Updating an non-existing pet should throw an error
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I unsuccessfully try to update a non-existing pet
      | categoryName | petName | photoUrls                                                        | tagName | status  |
      | Cat          | Eek     | https://static.tvtropes.org/pmwiki/pub/images/eekthecat_1947.jpg | tag2    | pending |
    Then the updated pet is not found in the petstore

  Scenario: Find pet by status
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I try to find all pets with status available
    Then my pet is returned

    #Bug found: Invalid status can be provided to api without errors
  Scenario: Finding a pet by an invalid status should throw an error
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I try to find all pets with status invalid
    Then response contains http status 400

#Bug found: Only first status is used to gather pets. All following statuses are discarded.
  Scenario: Find pet by multiple statuses
    Given I have the following pets
      | categoryName | petName  | photoUrls                                                                                                                                | tagName | status    |
      | Dog          | Lassie   | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg                                                                               | tag1    | available |
      | Cat          | Garfield | https://vignette.wikia.nocookie.net/garfield/images/9/9f/GarfieldCharacter.jpg/revision/latest/scale-to-width-down/620?cb=20180421131132 | tag2    | sold      |
    And I successfully add the pets to the petstore
    When I try to find all pets with status available,sold
    Then my pets are returned

  #Bug found: Only first tag is used to gather pats. Second one is discarded
  Scenario: Find pet by multiple tags [DEPRECATED]
    Given I have the following pets
      | categoryName | petName  | photoUrls                                                                                                                                | tagName | status    |
      | Dog          | Lassie   | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg                                                                               | tag1    | available |
      | Cat          | Garfield | https://vignette.wikia.nocookie.net/garfield/images/9/9f/GarfieldCharacter.jpg/revision/latest/scale-to-width-down/620?cb=20180421131132 | tag2    | sold      |
    And I successfully add the pets to the petstore
    When I try to find all pets with tags tag1,tag2
    Then my pets are returned

  Scenario: Update an existing pet with form data
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I successfully update the existing pet name and status to the following data
      | petName | status  |
      | Eek     | pending |
    Then the updated pet is found in the petstore

  Scenario: Delete a pet
    Given I have the following pet
      | categoryName | petName | photoUrls                                                  | tagName | status    |
      | Dog          | Lassie  | https://theoptimist.nl/app/uploads/2018/08/Lassie_crop.jpg | tag1    | available |
    And I successfully add the pet to the petstore
    When I successfully delete the pet from the petstore
    Then the pet is not found in the petstore

  Scenario: Delete a non-existing pet
    Then I unsuccessfully delete a non-existing pet