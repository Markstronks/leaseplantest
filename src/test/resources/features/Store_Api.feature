Feature: Store Api

  #Question: How to update order status? No api endpoint available

  Scenario: Place an order for a pet
    Given the following order for a pet
      | petId | quantity | shipDate                     | status | complete |
      | 1     | 1        | 2020-09-04T19:42:07.501+0000 | placed | true     |
    When I successfully place the order
    Then the order is found in the petstore

  #Bug found: Order can be placed with an invalid status
  Scenario: Placing an order for a pet with an invalid status should throw an error
    Given the following order for a pet
      | petId | quantity | shipDate                     | status  | complete |
      | 1     | 1        | 2020-09-04T19:42:07.501+0000 | invalid | true     |
    When I unsuccessfully place the order
    Then the order is not found in the petstore

  Scenario: Delete purchase order by id
    Given the following order for a pet
      | petId | quantity | shipDate                     | status | complete |
      | 1     | 1        | 2020-09-04T19:42:07.501+0000 | placed | true     |
    And I successfully place the order
    When I successfully delete the order
    Then the order is not found in the petstore

  Scenario: Delete a non-existing purchase order by id
    Then I unsuccessfully delete a non-existing order

  Scenario: Request pet inventories by status
    When I request the pet inventory
    Then the pet inventory is successfully retrieved