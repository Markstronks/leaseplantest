Feature: User Api

  Scenario: Create a single user
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    When I add the single user
    Then the user can be found

  Scenario: Create multiple users as an Array
    Given the following users
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName2 | lastName2 | email2 | password2 | phone2 | 2          |
      | firstName3 | lastName3 | email3 | password3 | phone3 | 3          |
      | firstName4 | lastName4 | email4 | password4 | phone4 | 4          |
      | firstName5 | lastName5 | email5 | password5 | phone5 | 5          |
    When I add users as an Array
    Then all users can be found

  Scenario: Create multiple users as a List
    Given the following users
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName2 | lastName2 | email2 | password2 | phone2 | 2          |
      | firstName3 | lastName3 | email3 | password3 | phone3 | 3          |
      | firstName4 | lastName4 | email4 | password4 | phone4 | 4          |
      | firstName5 | lastName5 | email5 | password5 | phone5 | 5          |
    When I add users as a List
    Then all users can be found

  Scenario: Update a user
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    And I add the single user
    When I update the user to the following values
      | firstName         | lastName         | email         | password         | phone         | userStatus |
      | firstName1Updated | lastName1Updated | email1Updated | password1Updated | phone1Updated | 2          |
    Then the updated user can be found

  #Bug found: Non-existing user can be updated. Should throw a 404 user not found
  Scenario: Updating a non-existing user should throw an error
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    And I add the single user
    When I unsuccessfully try to update a non-existing user
      | firstName         | lastName         | email         | password         | phone         | userStatus |
      | firstName1Updated | lastName1Updated | email1Updated | password1Updated | phone1Updated | 2          |
    Then the updated user cannot be found


  Scenario: Delete a user
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    And I add the single user
    When I successfully delete the user
    Then the user cannot be found

  Scenario: Deleting a non-existing user should throw an error
    Then I unsuccessfully delete a non-existing user

  Scenario: User logs in to system
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    When the user logs in to the system
    Then a user session is returned

    #Bug found: User can login with invalid password
  Scenario: User logging in to system with a wrong password should throw an error
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    When the user logs in to the system with the wrong password
    Then no user session is returned


    #Question: This api call ends the current usersession. This will not work well with multiple users logged in at the same time.
    # Better add the username as parameter to the api call.
  Scenario: Log out current logged in user
    Given the following user
      | firstName  | lastName  | email  | password  | phone  | userStatus |
      | firstName1 | lastName1 | email1 | password1 | phone1 | 1          |
    And the user logs in to the system
    And a user session is returned
    When the user logs out
    Then the session has ended