# How to compile the test

Use the following command at the terminal:

```bash
mvn clean compile
```

# How to run the test

Use the following command at the terminal:

```bash
mvn verify
```

# How to run a specific test

Usually all tests are run when giving the command mvn verify.
To run a specific test you can create a tag (e.g. @run) and add this above the scenario(s) that you would like to run.

Then add the tag to the SerenityRunnerIT class:

```bash
@CucumberOptions(features="src/test/resources/features",
        tags = "@run",
        glue= "cucumber/steps")
```

# How to write new tests

First determine what part of the application you need to write a test for.
When it concerns Pets:
 - Add the scenarios to the feature file Pet_Api.feature 
 - Add the corresponding step definitions in the PetStepDefs class
 
When it concerns the Store:
 - Add the scenarios to the feature file Store_Api.feature
 - Add the corresponding step definitions in the StoreStepDefs class
  
When it concerns Users:
 - Add the scenarios to the feature file User_Api.feature 
 - Add the corresponding step definitions in the UserStepDefs class
 
When it concerns something else:
 - Create a new feature file in: 
 ```bash
 src/test/resources/features/
 ```
 - Create a new step definition class in:   
```bash
src/test/java/cucumber/steps
```
 - For comparing results it is best to use objects. When you need to create a new object you can place it at:
  ```bash
  src/test/java/cucumber/entities
  ```

# Test Endpoint

The endpoints used for testing are located at: [petstore.swagger.io](https://petstore.swagger.io/).
There you can also find more documentation about the APIs.

# Bugs, questions and improvements

In the feature files some bugs, questions and improvements are noted in comments. These should be discussed with the developer/team.

